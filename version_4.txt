The slings and arrows of outrageous fortune,
Or to take Arms against a Sea of troubles,
And by opposing end them: to die, to sleep
No more; and by a sleep, to say we end
The heart-ache, and the thousand natural shocks
That Flesh is heir to? 'Tis a consummation
The fair Ophelia? Nymph, in thy Orisons
Be all my sins remember'd.[1]
__________________________________________________