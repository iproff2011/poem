To be, or not to be, Ay there's the point,
To Die, to sleep, is that all? Aye all:
No, to sleep, to dream, aye marry there it goes,
For in that dream of death, when we awake,
And borne before an everlasting Judge,
From whence no passenger ever returned,
The undiscovered country, at whose sight
The happy smile, and the accursed damn'd.
But for this, the joyful hope of this,
Who'd bear the scorns and flattery of the world,
Scorned by the right rich, the rich cursed of the poor?
The widow being oppressed, the orphan wrong'd,
The taste of hunger, or a tyrants reign,